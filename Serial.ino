
/*
 *    ||===========================================||
 *    || Name : Simple Serial Dual Servo Control   ||
 *    || Author : Shane Francis                    ||
 *    || Student ID : FRA09165298                  ||
 *    || Version : 1.1                             ||
 *    || Description :                             ||
 *    ||    A simple serial servo control program  ||
 *    ||    which also supports the HC-SR04 sonar  ||
 *    ||    module                                 ||
 *    ||===========================================||
 */

// Simple Dual Servo Control Via Serial
#include <Servo.h>

//Define the pins that will be used for signals
#define receivePIN 5 
#define echoPin 6
#define servoApin 10
#define servoBpin 11


char servoData[10]; //create a small string to store the servo data
char servoName; //Storage for the servo name
bool printNeed = false ; //Storage for if we need to print to serial
int availabledata = 0; //Allows storage of how much data is in the serial buffer
Servo servoA, servoB; //Aervi A is steering || Servo B is motor / ESC


//Setup
void setup() {

  servoB.attach(servoBpin); //attached servoB
  servoB.write(1); //write 1 to servoB to initiallise ESC
  delay(5000); //Wait 5 sec for the ESC to be ready (this reduces the chance of fail-safe)


  servoA.attach(servoApin); //Attach servoB
  servoA.write(70); //Center servo (70 seems more center than 90 ....)

  pinMode(receivePIN, OUTPUT); //set up the reciver for the sonar module
  pinMode(echoPin, INPUT); //setup the transmitter for the sonar module

  delay(500); //Wait 0.5 seconds for the setup to complete.

  Serial.begin(9600);  //Start Serial at b9600

  Serial.print('I'); //Inform the host that we are ready
}

//Infinite control loop
void loop() {
  

   availabledata = Serial.available(); //Assing the number of bytes available on serial
   servoName = '/0'; //Null terminate the servo name string
   printNeed = false;   //We dont need to prin

    //only if there is more than 4 bytes on the serial bus can we process the data
    if (availabledata > 4){
        //Serial.println(availabledata); 
        servoName = Serial.read(); //read the 1st Byte into servo data

        //while starting at 1 loop through all the available data
        for(int count = 1; count < availabledata; count++){ 
          servoData[count-1] = Serial.read(); //read all data from the 2nd byte into n-1 of servodata
        }
        printNeed = true; //we now need to print 

        servoData[availabledata-1] = '/0'; //we null off the string data at the correct place

        //if the host is using the correct CMD structure this sould NEVER happen
        while (Serial.available())
            Serial.read(); //in case anything is left on the serial bus read clear it

    }

    int real = atoi(servoData); //convert the 3 bytes of servo data into an INT

    long distance = 50; //Create a local variable to store the range and initialise it
    distance = getDistance(); //get the distance from the sonar

    if (servoName == 'A'){ //If the 1st command byte was A
      
        if (distance > 25) { //if the sonar mesured over 25CM
            Serial.print('A'); //confirm that the data was recived
        } else{
            Serial.print('Z'); //else warn the host of impending doom
        }

        /*
         * ToDo :
         *       We should report a failur to set the PWM signal
         *       to the host
         */
        servoA.write(real); //write the converted data to the servo
 
    }
    
    if (servoName == 'B'){ //If the 1st command byte was B

        if (distance > 25) { //if the sonar mesured over 25CM
            Serial.print('B'); //confirm that the data was recived
        } else{
            Serial.print('Z'); //else warn the host of impending doom
        }       
        
        /*
         * ToDo :
         *       We should report a failur to set the PWM signal
         *       to the host
         */
        servoB.write(real); //write the converted data to the ESC
 
    }

}


/*
 *    ||============================================================||
 *    ||Get distance from the HC-SR04 sonar unit using              ||
 *    ||the modules datasheet, this can be found at :-              ||
 *    ||http://jaktek.com/wp-content/uploads/2011/12/HC-SR04.pdf    ||
 *    ||============================================================||
 */
long getDistance() {

  int ping_time = 50; //initialise the veriable for ping time
  
  long time, range; //reate long intagers for time and range values


  digitalWrite(receivePIN, LOW);  //Make sure the trigger pin is low and ping is waiting
  delayMicroseconds(ping_time);   //Wait the ping_time for any pulses to pass

  digitalWrite(receivePIN, HIGH);  //Pring the trigger pin high for to create a pulse
  delayMicroseconds(ping_time); //wait for ping_time, this creates a solid ping


  time = pulseIn(echoPin, HIGH);  //recorde the time untill we recive the pulse back
  time = time + ping_time; //compensate for the 50 microseconds pulse

  /*
   * calculate the distance in cm from the object using the speed of sound as
   * 340m/s ..... or 290m/s for when compensating for errors in timing 
   */
  range = (time/2) * 0.0290;

  return range; //return the range
}

